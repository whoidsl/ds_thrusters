/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivandor on 8/29/19.
//

#ifndef PROJECT_TECNADYNE561_STRUCTS_H
#define PROJECT_TECNADYNE561_STRUCTS_H

namespace ds_thrusters {
    namespace tecnadyne_structs {

        struct header {
            uint8_t DLE;
            uint8_t STX;
            uint8_t STN;
            uint8_t CMD;
        } __attribute__((packed));

        struct extended_status {
            uint8_t speed_loop_enable_status;
            uint8_t minute_tag;
            uint8_t second_tag;
            uint8_t microsecond_tick_tag;
            int8_t prop_dir;
            uint8_t prop_pwm;
            uint8_t tach_msb;
            uint8_t tach_lsb;
            uint8_t fault_status;
            uint8_t reset_status;
            uint8_t speed_loop_interval;
            uint8_t tach_err_count;
            uint8_t voltage_msb;
            uint8_t voltage_lsb;
            uint8_t current_msb;
            uint8_t current_lsb;
        } __attribute__((packed));


        struct footer {
            uint8_t DLE;
            uint8_t ETX;
            uint8_t BCC;
        };
    } // tecnadyne561_structs
} //ds_thrusters
#endif //PROJECT_TECNADYNE561_STRUCTS_H
