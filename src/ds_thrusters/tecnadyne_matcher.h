//
// Created by ivandor on 11/1/19.
//

#ifndef DS_THRUSTERS_TECNADYNE_MATCHER_H
#define DS_THRUSTERS_TECNADYNE_MATCHER_H
class tecnadyne_matcher
{
public:
    explicit tecnadyne_matcher()
    {
        state = SEARCHING;
    }

    enum MatcherState {
            SEARCHING = 0,
            DLE_FOUND,
            ETX_FOUND,
    };
    MatcherState state;

    typedef boost::asio::buffers_iterator<boost::asio::streambuf::const_buffers_type> iterator;
    template <typename Iterator>
    std::pair<Iterator, bool> operator()(Iterator begin, Iterator end) {
        Iterator iter = begin;
        for (iter=begin; iter!=end;iter++) {
            switch (state) {
                case SEARCHING:
                    if (*iter == 0x10) {
                        state = DLE_FOUND;
                    }
                    break;
                case DLE_FOUND:
                    if (*iter == 0x03) {
                        state = ETX_FOUND;
                    } else {
                        state = SEARCHING;
                    }
                    break;
                case ETX_FOUND:
                    // this is a checksum byte; consume it, reset, and return;
                    iter++;
                    state = SEARCHING; // for the next one
                    return std::make_pair(iter, true);
                default:
                    state = SEARCHING;
                    ROS_ERROR_STREAM("This should never get reached since etx should return");

            }
        }
        return std::make_pair(iter, false);
    }
};
#endif //DS_THRUSTERS_TECNADYNE_MATCHER_H
