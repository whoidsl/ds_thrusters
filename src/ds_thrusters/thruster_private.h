/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivandor on 11/1/19.
//

#ifndef DS_THRUSTERS_THRUSTER_PRIVATE_H
#define DS_THRUSTERS_THRUSTER_PRIVATE_H

#include "ds_thrusters/thruster.h"

namespace ds_thrusters {
/* struct ThrusterPrivate {
  uint8_t address_;
  uint64_t iosm_setpoint_cmdid_;
  uint64_t iosm_enable_cmdid_;
  double max_ttl_;
  double thruster_artificial_delay_;
  int thruster_idx_;
  int thruster_dir_;
  std::string command_topic_;
  std::string tecnadynestate_topic_;
  std::string thrusterstate_topic_;
  ros::Time cmd_timeout_;
  ds_actuator_msgs::Tecnadyne561 tecnadyne_state;
  ds_actuator_msgs::ThrusterState thruster_state;
  ros::Publisher tecnadyne_pub_;
  ros::Publisher thruster_pub_;
  ros::Subscriber command_sub_;


}; */

// tach-counts-to-RPM
const float COUNTS_TO_RPS_561 = 144.0f;

struct TecnadyneCmd {
  uint8_t dle1;
  uint8_t soh; // 0x01 for master, 0x02 for slave
  uint8_t addr; // aka stn

  uint8_t cmd; // = 0x41

  uint8_t db1;
  uint8_t db2;
  uint8_t db3;
  uint8_t db4;

  uint8_t dle2;
  uint8_t etx;
  uint8_t bcc;
} __attribute__((packed));

struct TecnadyneDriverPrivate {
  /// \brief Loaded from param server
  TecnadyneDriverPrivate() {
    system_enable = false;
    closed_loop = false;
  }
  //std::vector<ThrusterPrivate> thrusters;

/*   ThrusterPrivate* getThrusterByAddr(int stn) {
      if (stn == address_) {
        return &(thrusters);
      }
    }
    ROS_ERROR_STREAM("INVALID STN ADDRESS: " <<stn);
    return nullptr;
  } */

  uint8_t address_;
  uint64_t iosm_setpoint_cmdid_;
  uint64_t iosm_enable_cmdid_;
  double max_ttl_;
  double thruster_artificial_delay_;
  int thruster_idx_;
  int thruster_dir_;
  std::string command_topic_;
  std::string tecnadynestate_topic_;
  std::string thrusterstate_topic_;
  ros::Time cmd_timeout_;
  ds_actuator_msgs::Tecnadyne561 tecnadyne_state;
  ds_actuator_msgs::ThrusterState thruster_state;
  ros::Publisher tecnadyne_pub_;
  ros::Publisher thruster_pub_;
  ros::Subscriber command_sub_;

  bool closed_loop;
  bool system_enable;
  int16_t cmd_max;
  uint8_t loop_time; // closed-loop integration time, in 5ms steps
  std::string abort_topic_;
  ros::Subscriber abort_topic_sub_;

  double cmd_timeout;

  /// @brief I/O state machine
  boost::shared_ptr<ds_asio::IoSM> iosm;

  /// \brief Connection
  boost::shared_ptr<ds_asio::DsConnection> tecnadyne_conn;

  static uint8_t thruster_checksum(const std::string& str, int omit=0) {
    uint32_t sum = 0;
    int end = static_cast<int>(str.size()) - omit;
    for (int i = 0; i < end; i++) {
      sum += static_cast<unsigned char>(str[i]);
    }

    return static_cast<uint8_t>(sum & 0xff);
  }

  static void stuff_dle(std::string& str) {

    // start on the byte before the end-of-message DLE
    // skip the start-of-message DLE
    for (ssize_t i=str.size()-4; i > 0; i--) {
      if (str[i] == '\x10') {
        str.insert(i, 1, '\x10');
      }
    }
  }

  static void unstuff_dle(std::string& str) {
    if (str.size() < 6) {
      // 5 is DLE + payload len 1 + DLE + STX + Checksum
      // There must be at least two payload characters to
      // have a stuffed DLE, so if there isn't return
      return;
    }
    // core concept: i points to the DLE we might be removing
    // we also need a simple 2-state FSM to decide if we're deleting or skipping
    bool dle_found = false;
    for (ssize_t i=str.size()-4; i > 0; i--) {
      if (str[i] == '\x10') {
        if (dle_found) {
          // we have two in a row! Delete this one!
          str.erase(i, 1);
          dle_found = false;
        } else {
          dle_found = true;
        }
      }
    }
  }

  static std::string make_thruster_pwm_cmd(int addr, int pwm_val) {
    std::string tecnadyne_pwm_cmd;
    if (pwm_val == 0) {
      tecnadyne_pwm_cmd = {ThrusterStateVariables::DLE, ThrusterStateVariables::SOH,
                           static_cast<int8_t>(addr),
                           ThrusterStateVariables::WriteCmd, ThrusterStateVariables::disable,
                           ThrusterStateVariables::db2, ThrusterStateVariables::db3,
                           ThrusterStateVariables::db4, ThrusterStateVariables::DLE,
                           ThrusterStateVariables::ETX};
    }
    else {
      tecnadyne_pwm_cmd = {ThrusterStateVariables::DLE, ThrusterStateVariables::SOH,
                           static_cast<int8_t>(addr), ThrusterStateVariables::WriteCmd,
                           ThrusterStateVariables::OpenLoopPwm, ThrusterStateVariables::OpenLoopPwm,
                           static_cast<int8_t>(pwm_val > 0 ? 0x00 : 0x01), static_cast<int8_t>(abs(pwm_val)),
                           ThrusterStateVariables::DLE, ThrusterStateVariables::ETX};
    }
    tecnadyne_pwm_cmd += thruster_checksum(tecnadyne_pwm_cmd);
    stuff_dle(tecnadyne_pwm_cmd);

    return tecnadyne_pwm_cmd;
  }

  static std::string make_thruster_loop_enable(int addr, bool enable) {
    // build the command
    TecnadyneCmd cmd;
    cmd.dle1 = ThrusterStateVariables::DLE;
    cmd.soh = ThrusterStateVariables::SOH;
    cmd.addr = addr;
    cmd.cmd = ThrusterStateVariables::WriteCmd;
    if (enable) {
      cmd.db1 = 1;
    } else {
      cmd.db1 = 0;
    }

    // remaining values are allegedly ignored; set to 0
    cmd.db2 = 0;
    cmd.db3 = 0;
    cmd.db4 = 0;

    cmd.dle2 = ThrusterStateVariables::DLE;
    cmd.etx = ThrusterStateVariables::ETX;

    cmd.bcc = 0; // This will cause the string conversion to stop just BEFORE this byte

    // convert to string
    std::string tecnadyne_loop_cmd(reinterpret_cast<char*>(&cmd), sizeof(TecnadyneCmd));

    // Add checksum & DLE stuff as required
    tecnadyne_loop_cmd.back() = thruster_checksum(tecnadyne_loop_cmd);
    stuff_dle(tecnadyne_loop_cmd);

    return tecnadyne_loop_cmd;
  }

  static std::string make_thruster_loop_cmd(int addr, int16_t setpoint, uint8_t loop_time) {

    // precompute some values
    float loop_time_sec = static_cast<float>(loop_time)*0.005;
    uint16_t tach_setpoint = static_cast<uint16_t>(round(static_cast<float>(abs(setpoint))*COUNTS_TO_RPS_561*loop_time_sec/60.0));
    if (tach_setpoint > 255) {
      ROS_WARN_STREAM("Tach setpoint > 255; reduce loop time to reach goal RPM=" <<setpoint);
      tach_setpoint = 255;
    }
    uint8_t dir = setpoint > 0 ? 0x00 : 0x01;

    // build the command
    TecnadyneCmd cmd;
    cmd.dle1 = ThrusterStateVariables::DLE;
    cmd.soh = ThrusterStateVariables::SOH;
    cmd.addr = addr;
    cmd.cmd = ThrusterStateVariables::WriteCmd;
    if (setpoint == 0) {
      // if set to zero, send an extra speedloop disable
      cmd.db1 = 0;
      cmd.db2 = 0;
      cmd.db3 = 0;
      cmd.db4 = 0;
    } else {
      // otherwise, do this for real
      cmd.db1 = ThrusterStateVariables::ClosedLoop;
      cmd.db2 = loop_time;
      cmd.db3 = dir;
      cmd.db4 = tach_setpoint;
    }

    // remaining values are canned
    cmd.dle2 = ThrusterStateVariables::DLE;
    cmd.etx = ThrusterStateVariables::ETX;
    cmd.bcc = 0; // so it isn't included in checksum

    // convert to string, skipping the checksum
    std::string tecnadyne_loop_cmd(reinterpret_cast<char*>(&cmd), sizeof(TecnadyneCmd));

    // Add checksum & DLE stuff as required
    tecnadyne_loop_cmd.back() = thruster_checksum(tecnadyne_loop_cmd);
    stuff_dle(tecnadyne_loop_cmd);

    return tecnadyne_loop_cmd;
  }

  void thruster_cmd_setup() {
    if (closed_loop) {
      std::string setpoint_str = make_thruster_loop_cmd(address_, 0, loop_time);
      std::string enable_str = make_thruster_loop_enable(address_, false);
      auto setpoint_cmd = ds_asio::IoCommand(setpoint_str, cmd_timeout);
      auto enable_cmd = ds_asio::IoCommand(enable_str, cmd_timeout);
      iosm_setpoint_cmdid_ = iosm->addRegularCommand(std::move(setpoint_cmd));
      iosm_enable_cmdid_ = iosm->addRegularCommand(std::move(enable_cmd));

    } else {
      std::string pwm_str = make_thruster_pwm_cmd(address_, 0);
      auto pwm_command = ds_asio::IoCommand(pwm_str, cmd_timeout);
      iosm_setpoint_cmdid_ = iosm->addRegularCommand(std::move(pwm_command));
    }
  }

  void thruster_disable() {
    if (closed_loop) {
      std::string setpoint_str = make_thruster_loop_cmd(address_, 0, loop_time);
      std::string enable_str = make_thruster_loop_enable(address_, false);
      auto setpoint_cmd = ds_asio::IoCommand(setpoint_str, cmd_timeout);
      auto enable_cmd = ds_asio::IoCommand(enable_str, cmd_timeout);
      iosm->overwriteRegularCommand(iosm_setpoint_cmdid_, setpoint_cmd);
      iosm->overwriteRegularCommand(iosm_enable_cmdid_, enable_cmd);
    } else {
      std::string pwm_str = make_thruster_pwm_cmd(address_, 0);
      auto pwm_command = ds_asio::IoCommand(pwm_str, cmd_timeout);
      iosm->overwriteRegularCommand(iosm_setpoint_cmdid_, pwm_command);
    }
  }

  void thruster_set_command(int16_t pwm_val) {
    if (pwm_val > cmd_max) {
      pwm_val = cmd_max;
    }
    if (pwm_val < -cmd_max) {
      pwm_val = -cmd_max;
    }
    if (closed_loop) {
      if (pwm_val == 0) {
        thruster_disable();
      } else {
        std::string setpoint_str = make_thruster_loop_cmd(address_, pwm_val, loop_time);
        std::string enable_str = make_thruster_loop_enable(address_, true);
        auto setpoint_cmd = ds_asio::IoCommand(setpoint_str, cmd_timeout);
        auto enable_cmd = ds_asio::IoCommand(enable_str, cmd_timeout);
        iosm->overwriteRegularCommand(iosm_setpoint_cmdid_, setpoint_cmd);
        iosm->overwriteRegularCommand(iosm_enable_cmdid_, enable_cmd);
      }

    } else {
      std::string pwm_str = make_thruster_pwm_cmd(address_, pwm_val);
      auto pwm_command = ds_asio::IoCommand(pwm_str, cmd_timeout);
      iosm->overwriteRegularCommand(iosm_setpoint_cmdid_, pwm_command);
    }
  }

};
}
#endif //DS_THRUSTERS_THRUSTER_PRIVATE_H
