/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivandor on 11/6/19.
//

#include "ds_thrusters/thruster.h"
#include "tecnadyne_matcher.h"
#include "thruster_private.h"

//#define ARTIFICIAL_DELAY
//#undef ARTIFICIAL_DELAY
#define STATUS_TIMEOUT 0.050
#define CMD_TIMEOUT 0.033

using namespace ds_thrusters;

TecnadyneDriver::TecnadyneDriver() : ds_base::DsProcess(), d_ptr_(std::unique_ptr<TecnadyneDriverPrivate>(new TecnadyneDriverPrivate))
{
}

TecnadyneDriver::TecnadyneDriver(int argc, char* argv[], const std::string& name) : ds_base::DsProcess(argc, argv, name),
                                                                                    d_ptr_(std::unique_ptr<TecnadyneDriverPrivate>(new TecnadyneDriverPrivate))
{
}

TecnadyneDriver::~TecnadyneDriver() = default;


// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXx
void TecnadyneDriver::setupParameters()
{
  ds_base::DsProcess::setupParameters();
  DS_D(TecnadyneDriver);
  ROS_INFO_STREAM("Reading thrusters from " <<ros::names::resolve("~num_thrusters"));
  int num_thrusters = ros::param::param<int>("~num_thrusters", 4);
  ROS_WARN_STREAM("Using " <<num_thrusters <<"thrusters");

  double max_thruster_ttl_ = ros::param::param<double>("~max_thruster_ttl", 60);
  double thruster_artificial_delay_ = ros::param::param<double>("~thruster_artificial_delay", 0.01);
  ROS_INFO_STREAM("Artificial delay  param: "<<thruster_artificial_delay_);
  d->cmd_timeout = CMD_TIMEOUT;
  d->closed_loop = ros::param::param<bool>("~closed_loop", true);
  d->loop_time = round(ros::param::param<float>("~loop_time", 0.030) / 0.005);
  // only difference is the sensible choice of defaults
  if (d->closed_loop) {
    d->cmd_max = ros::param::param<int>("~cmd_max", 3200);
    ROS_INFO_STREAM("Tecnadyne driver running in CLOSED LOOP RPM mode");
    ROS_INFO_STREAM("Using loop time=" <<static_cast<int>(d->loop_time));
  } else {
    d->cmd_max = ros::param::param<int>("~cmd_max", 255);
    ROS_INFO_STREAM("Tecnadyne driver running in OPEN LOOP PWM mode");
  }
  ROS_INFO_STREAM("Max command: " <<d->cmd_max);

    std::string thruster_name = ros::param::param<std::string>("~/descriptive_name", "thrusterN");
    ROS_WARN_STREAM("Thruster name is: "<<thruster_name);
    ROS_INFO_STREAM("Reading address from " <<ros::names::resolve("~/"
                                                                      + thruster_name + "/thruster_address"));
    std::string raw_addr_ = ros::param::param<std::string>("~/" + thruster_name + "/thruster_address", "0x64");
    d->address_ = std::stoi(raw_addr_, nullptr, 0);
    ROS_INFO_STREAM("Thruster "<<thruster_name<<" has addr=0x" <<std::hex <<static_cast<int>(d->address_));
    d->max_ttl_ = max_thruster_ttl_;
    d->thruster_artificial_delay_ = thruster_artificial_delay_;
    d->thruster_dir_ = ros::param::param<int>("~/" + thruster_name + "/thruster_dir", 1.0);
    if (d->thruster_dir_ != 1 && d->thruster_dir_ != -1) {
      ROS_ERROR_STREAM("Thruster direction MUST be 1 or -1");
      ROS_BREAK();
    }
    d->command_topic_ = ros::param::param<std::string>("~/" + thruster_name + "/command_topic",
                                                                    thruster_name + "/cmd");
    d->tecnadynestate_topic_ = ros::param::param<std::string>("~/" + thruster_name + "/tecnadynestate_topic",
                                                                           thruster_name + "/tecnadynestate");
    d->thrusterstate_topic_ = ros::param::param<std::string>("~/" + thruster_name+ "/thrusterstate_topic",
                                                                          thruster_name + "/state");

    d->abort_topic_ = ros::param::param<std::string>("~abort_topic", "abort");
    ROS_INFO_STREAM("params set");
}

void TecnadyneDriver::setupTimers() {
  ds_base::DsProcess::setupTimers();
  DS_D(TecnadyneDriver);
  //d->thruster_cmd_state_timer_ = nodeHandle().createTimer(ros::Duration(1.0), boost::bind(&TecnadyneDriver::checkProcessStatus, this, _1));
  ROS_INFO_STREAM("timers set");
}

void TecnadyneDriver::setupConnections() {
  ds_base::DsProcess::setupConnections();
  DS_D(TecnadyneDriver);

  d->iosm = addIoSM("statemachine", "tecnadyne_conn");

  auto conn = boost::dynamic_pointer_cast<ds_asio::DsSerial>(d->iosm->getConnection());
  if (conn) {
    conn->set_matcher(tecnadyne_matcher());
    ROS_INFO_STREAM("Overriding default matcher function");
  }

  //setup commands
  //for (int i=0; i<d->thrusters.size();i++) {
    tecnadyne_status_cmd = {ThrusterStateVariables::DLE, ThrusterStateVariables::SOH,
                                        static_cast<char>(d->address_), ThrusterStateVariables::Status,
                                        ThrusterStateVariables::DLE, ThrusterStateVariables::ETX};
    tecnadyne_status_cmd += d->thruster_checksum(tecnadyne_status_cmd);
    //ROS_INFO_STREAM("STATUS CMD: "<<tecnadyne_status_cmd);
    auto status_command = ds_asio::IoCommand(tecnadyne_status_cmd, STATUS_TIMEOUT, false, boost::bind(&TecnadyneDriver::parseReceivedBytes, this, _1));
    status_command.stateTransErr = false;
    status_command.setDelayAfter(ros::Duration(d->thruster_artificial_delay_));
    d->iosm->addRegularCommand(std::move(status_command));

    d->thruster_cmd_setup();
  //}
}

void TecnadyneDriver::checkProcessStatus(const ros::TimerEvent& event) {
  DS_D(TecnadyneDriver);
  const auto now = ros::Time::now();

    if (ros::Time::now() > d->cmd_timeout_) {
      d->thruster_disable();
    }
  }

void TecnadyneDriver::setupPublishers() {
  ds_base::DsProcess::setupPublishers();
  DS_D(TecnadyneDriver);

  // prepare our publishers
  auto nh = nodeHandle();

  //tecnadyne state pubs
    d->tecnadyne_pub_ = nh.advertise<ds_actuator_msgs::Tecnadyne561>(d->tecnadynestate_topic_, 10);
    d->thruster_pub_ = nh.advertise<ds_actuator_msgs::ThrusterState>(d->thrusterstate_topic_, 10);
  ROS_INFO_STREAM("publishers set");
}

void TecnadyneDriver::setupSubscriptions() {
  ds_base::DsProcess::setupSubscriptions();
  DS_D(TecnadyneDriver);
  auto nh = nodeHandle();

    d->command_sub_ = nh.subscribe<ds_actuator_msgs::ThrusterCmd>(
        d->command_topic_, 10,
        boost::bind(&TecnadyneDriver::cmdRecv, this, _1));
  // listen for abort status
  d->abort_topic_sub_ = nh.subscribe(d->abort_topic_,10, &TecnadyneDriver::setEnable, this);

  ROS_INFO_STREAM("subscriptions set");
}
// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXx
///*---------------------------------------------------------------------------------------------*///
///*   parseReceivedBytes: Takes incoming raw data, determines thruster id, parse, & publishes   *///
///*---------------------------------------------------------------------------------------------*///
bool TecnadyneDriver::parseReceivedBytes(const ds_core_msgs::RawData& bytes) {
  DS_D(TecnadyneDriver);

  // try to decode based on structure definition
  const uint8_t* buffer = bytes.data.data();
  
  // Convert to string
  std::string buffer_msg(reinterpret_cast<const char*>(bytes.data.data()), bytes.data.size());
 
  // DLE stuffing happens outside the checksum
  d->unstuff_dle(buffer_msg);

  // check the checksum
  auto calc_checksum = d->thruster_checksum(std::string(buffer_msg.begin(), buffer_msg.end()), 1); // exclude the received checksum
  // recv checksum should be the last byte
  auto recv_checksum = bytes.data.back();
  
  echoed_msg = false; //Set a flag to catch echoed msgs
  if (buffer_msg[1] != ThrusterStateVariables::STX) {
	  echoed_msg = true;
  }
  
  //ROS_WARN_STREAM("echo for : "<<ros::this_node::getName()<<": "<<echoed_msg);
  
  // Pass processed buffer to parseMessage if checksums match
  if (recv_checksum == calc_checksum) {	  
     if (!echoed_msg) {
	// go ahead and parse the message
        return parseMessage(bytes, buffer_msg);
     }
     return false;
  } else {
    ROS_WARN_STREAM("Thruster checksum mismatch! recv_checksum: "
                        <<static_cast<int>(recv_checksum) << " and calc_checksum: "
                        << static_cast<int>(calc_checksum));
    return true;
  }
}

bool TecnadyneDriver::parseMessage(const ds_core_msgs::RawData& bytes, std::string& raw_msg) {
  DS_D(TecnadyneDriver);

  auto *hdr = reinterpret_cast<const ds_thrusters::tecnadyne_structs::header *>(raw_msg.data());
  auto *msg = reinterpret_cast<const ds_thrusters::tecnadyne_structs::extended_status *>(raw_msg.data() +
      sizeof(ds_thrusters::tecnadyne_structs::header));

  // deal with any escaping
  if (raw_msg.data() == tecnadyne_status_cmd) {
	 return false;
  } 
  //ROS_ERROR_STREAM(raw_msg.data()[1]);
  // ... check header message fields
  if (hdr->DLE != ThrusterStateVariables::DLE || hdr->STX != ThrusterStateVariables::STX) {
    ROS_ERROR_STREAM("INVALID DLE or STX");
    return false;
  }

    // fill in full logging thruster message based on new data
    d->tecnadyne_state.header = bytes.header;
    d->tecnadyne_state.ds_header = bytes.ds_header;
    d->tecnadyne_state.speed_loop_enable_status = msg->speed_loop_enable_status;
    d->tecnadyne_state.minute_tag = msg->minute_tag;
    d->tecnadyne_state.second_tag = msg->second_tag;
    d->tecnadyne_state.microsecond_tick_tag = msg->microsecond_tick_tag;
    double prop_dir = 1.0;
    if (msg->prop_dir == 1) {
      prop_dir = -1.0;
    }
    d->tecnadyne_state.prop_pwm = static_cast<int16_t>(msg->prop_pwm) * prop_dir;
    float thruster_rpm = static_cast<float>((static_cast<int32_t>(msg->tach_msb) << 8)
        + static_cast<int32_t>(msg->tach_lsb));
    //std::cout<<thruster_rpm<<std::endl;

    if (d->closed_loop) {
      // for whatever reason, in closed-loop mode RPM is relative to the period
      d->tecnadyne_state.tach_rpm = ((thruster_rpm * 60.0) / (COUNTS_TO_RPS_561 * 0.005 * static_cast<float>(d->loop_time))) * prop_dir;
    } else {
      d->tecnadyne_state.tach_rpm = ((thruster_rpm * 60.0) / COUNTS_TO_RPS_561) * prop_dir;
    }

    d->tecnadyne_state.fault_status = msg->fault_status;
    d->tecnadyne_state.reset_status = msg->reset_status;
    d->tecnadyne_state.speed_loop_interval = msg->speed_loop_interval;
    d->tecnadyne_state.tach_err_count = msg->tach_err_count;

    // Convert voltage to volts
    d->tecnadyne_state.voltage_volts = static_cast<float>((static_cast<int32_t>(msg->voltage_msb) << 8)
        + static_cast<int32_t>(msg->voltage_lsb)) * .02475; // see tecnadyne561 doc for conversion
    // Convert current to amps
    d->tecnadyne_state.current_amps = static_cast<float>((static_cast<int32_t>(msg->current_msb) << 8)
        + static_cast<int32_t>(msg->current_lsb)) * .02475; // see tecnadyne561 doc for conversion

    // actually publish
    d->tecnadyne_pub_.publish(d->tecnadyne_state);

    // fill in ThrusterState from the full message
    d->thruster_state.header = d->tecnadyne_state.header;
    d->thruster_state.ds_header = d->tecnadyne_state.ds_header;
    if (d->closed_loop) {
      d->thruster_state.actual_value = d->tecnadyne_state.tach_rpm;
    } else {
      d->thruster_state.actual_value = d->tecnadyne_state.prop_pwm;
    }

    //thruster->thruster_state.actual_value = thruster->tecnadyne_state.prop_pwm;
    // Commanded value updates in cmdRecv callback
    d->thruster_state.enable = d->system_enable;
    d->thruster_pub_.publish(d->thruster_state);
  //}
  // All desired state messages are published.  Success!
  return true;
}
void TecnadyneDriver::cmdRecv(const boost::shared_ptr<const ds_actuator_msgs::ThrusterCmd>& msg) {
  DS_D(TecnadyneDriver);
  //ROS_ERROR_STREAM("IN command recv, thruster IDX=" <<thruster_idx_);

  // get the thruster structure
  //auto & thruster = d->thrusters[thruster_idx_];

  double ttl = msg->ttl_seconds;
  if (ttl > d->max_ttl_) {
    ttl = d->max_ttl_;
  }
  d->cmd_timeout_ = ros::Time::now() + ros::Duration(ttl);
  //ROS_WARN_STREAM("THRUSTER CMD TIMEOUT: "<<thruster.cmd_timeout_);
  int pwm_val = round(msg->cmd_value * d->thruster_dir_);
  // update the commanded value
  std::string tecnadyne_pwm_str;
  if (d->system_enable && ttl > 0) {
    d->thruster_set_command(pwm_val);
  } else {
    // we're disabled! Send zero!
    d->thruster_disable();
  }
  d->thruster_state.cmd_value = pwm_val;
  d->tecnadyne_state.cmd = pwm_val;
}

void TecnadyneDriver::setEnable(const ds_core_msgs::Abort& abort) {
  DS_D(TecnadyneDriver);
  d->system_enable = abort.enable;

  if (!abort.enable) {
    ROS_ERROR_STREAM_ONCE("Thruster Driver not enabled");
    // overwrite all thruster regular commands
      d->thruster_disable();
    }

    // send zero to broadcast immediately, just in case
    ROS_ERROR_STREAM_ONCE("SENDING ZERO TO BROADCAST");
    auto tecnadyne_pwm_disable_str = d->make_thruster_pwm_cmd(ThrusterStateVariables::BroadcastAddr, 0);
    auto pwm_disable_cmd = ds_asio::IoCommand(tecnadyne_pwm_disable_str, 0.25);
    d->iosm->addPreemptCommand(pwm_disable_cmd);

  }
