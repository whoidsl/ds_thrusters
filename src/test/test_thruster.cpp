/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivandor on 08/23/19.
//

#include "ds_thrusters/thruster.h"
#include <ds_util/ds_util.h>
#include <gtest/gtest.h>
#include "../src/ds_thrusters/thruster_private.h"

// Test fixture for parsing, needed because we want to call ros::Time::init() before our tests.
class ThrusterTest : public ::testing::Test
{
public:
  // This method runs ONCE before a text fixture is run (not once-per-test-case)
  static void SetUpTestCase()
  {
    ros::Time::init();
  }
};

///////////////////////////////////////////////////////////////////////////////
// ThrusterModule
///////////////////////////////////////////////////////////////////////////////

/*
TEST_F(ThrusterTest, AcceptsGood_2)
{
  ds_thrusters::Thruster this_thruster;
  std::string testStr;
  testStr = ("0x10\x02\x6F\x37\x10\x03\x1B");
  EXPECT_TRUE(this_thruster.handleMessage(testStr));
  testStr = ("\x10\x02\x6F\x37\x00\x00\x00\x00\x10\x03\x1B");
  EXPECT_TRUE(this_thruster.handleMessage(testStr));
}

TEST_F(ThrusterTest, ParsesCounts)
{
  ds_thrusters::Thruster this_thruster("thruster64", 255, 60, "11");
  ASSERT_TRUE(this_thruster.handleMessage("\x10\x02\x6F\x37\x00\x00\x00\x00\x10\x03\x1B"));
  EXPECT_FLOAT_EQ(this_thruster.get_addr(), 16.0);
  EXPECT_EQ(this_thruster.get_prop_pwm(), 20.0);
}
 */

TEST(TecnadyneInternals, dleStuffStart) {
  std::string msg("\x10\x01\x6F\x10\x01\x02\x03\x10\x05\x06\x07\x10\x03\x1B");
  std::string expected("\x10\x01\x6F\x10\x10\x01\x02\x03\x10\x10\x05\x06\x07\x10\x03\x1B");

  ds_thrusters::TecnadyneDriverPrivate::stuff_dle(msg);

  EXPECT_EQ(expected, msg);
}

TEST(TecnadyneInternals, dleStuffEnd) {
  std::string msg("\x10\x01\x6F\x00\x01\x02\x03\x10\x05\x06\x10\x10\x03\x1B");
  std::string expected("\x10\x01\x6F\x00\x01\x02\x03\x10\x10\x05\x06\x10\x10\x10\x03\x1B");

  ds_thrusters::TecnadyneDriverPrivate::stuff_dle(msg);

  EXPECT_EQ(expected, msg);
}

TEST(TecnadyneInternals, dleUnstuffStart) {
  // just reverse the previous test
  std::string msg ("\x10\x01\x6F\x10\x10\x01\x02\x03\x10\x10\x05\x06\x07\x10\x03\x1B");
  std::string expected("\x10\x01\x6F\x10\x01\x02\x03\x10\x05\x06\x07\x10\x03\x1B");

  ds_thrusters::TecnadyneDriverPrivate::unstuff_dle(msg);

  EXPECT_EQ(expected, msg);
}

TEST(TecnadyneInternals, dleUnstuffEnd) {
  std::string msg("\x10\x01\x6F\x00\x01\x02\x03\x10\x10\x05\x06\x10\x10\x10\x03\x1B");
  std::string expected("\x10\x01\x6F\x00\x01\x02\x03\x10\x05\x06\x10\x10\x03\x1B");

  ds_thrusters::TecnadyneDriverPrivate::unstuff_dle(msg);

  EXPECT_EQ(expected, msg);
}

TEST(TecnadyneInternals, dleUnstuffRepeat) {
  // there's a potential bug with 3 or more escape DLE's right next to eachother.  Check for it!
  std::string msg("\x10\x01\x6F\x10\x10\x10\x10\x10\x10\x05\x06\x10\x10\x10\x03\x1B");
  std::string expected("\x10\x01\x6F\x10\x10\x10\x05\x06\x10\x10\x03\x1B");

  ds_thrusters::TecnadyneDriverPrivate::unstuff_dle(msg);

  EXPECT_EQ(expected, msg);
}
// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
