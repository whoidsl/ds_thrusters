# ds_thrusters

A repository for thruster nodes and associated files

## catkin packages

* tecnadyne561

## Current Status (as of 09/12/19):
The thruster driver currently supports parsing tecnadyne561 extended status messages and is set up to
poll for these regularly. It is also configured to send PWM commands via the ThrusterCommander.srv file
in ds_thruster_msgs. There is support for subscribing to the ThrusterCmd topic and using the pwm value
published there, but this is not currently turned on since testing it requires a mission controller publishing
thruster cmd_value's. There is also support for listening to the abort msg and the current behavior is to send a
PWM stop message if abort is enabled.

I have not yet fully implemented support for the 8 Thrusters Power message, which allows you to assign individual pwm
values and directions to each of the thrusters at one time. There is a basic structure for this in the code, but I don't
have the bitmasking for direction fully fleshed out. The next steps there are to take the desired pwm value, determine
direction based on its sign, and then pass that direction into a function that creates the mask based on desired direction
for each thruster address.

Also, I need to develop appropriate tests for all of the thruster behavior. Yeah.
