/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivandor on 08/19/19.
//

#ifndef PROJECT_THRUSTERS_H
#define PROJECT_THRUSTERS_H

#include <ds_base/ds_process.h>
#include "ds_actuator_msgs/Tecnadyne561.h"
#include "ds_core_msgs/IoSMcommand.h"
#include "ds_actuator_msgs/ThrusterState.h"
#include "ds_actuator_msgs/ThrusterCmd.h"
#include "ds_core_msgs/Abort.h"
#include "../../src/ds_thrusters/tecnadyne561_structs.h"
#include <string>
#include <memory>
#include <ds_base/util.h>

namespace ds_thrusters
{
struct TecnadyneDriverPrivate;

class TecnadyneDriver : public ds_base::DsProcess
{
    DS_DECLARE_PRIVATE(TecnadyneDriver)

public:
  explicit TecnadyneDriver();
  TecnadyneDriver(int argc, char* argv[], const std::string& name);
  ~TecnadyneDriver() override;
  DS_DISABLE_COPY(TecnadyneDriver)
protected:
  /// \function overrides from DsProcess
  void setupParameters() override;
  void setupTimers() override;
  void setupConnections() override;
  void setupPublishers() override;
  void setupSubscriptions() override;

  bool parseReceivedBytes(const ds_core_msgs::RawData& bytes);
  bool parseMessage(const ds_core_msgs::RawData& bytes, std::string& raw_msg);
  
  std::string tecnadyne_status_cmd;
  bool echoed_msg;
private:
    std::unique_ptr<TecnadyneDriverPrivate> d_ptr_;

  /// \brief sends stop commands to thrusters
  //void stop(int thruster_idx_);

  /// \brief Listens for abort commands to set enable flag
  void setEnable(const ds_core_msgs::Abort& abort);

  /// \brief Listens for thruster cmds
  //void _command_req(const ds_actuator_msgs::ThrusterCmd& msg);

  /// \brief packages and sends thruster cmds to individual thrusters
  void cmdRecv(const boost::shared_ptr<const ds_actuator_msgs::ThrusterCmd> & msg);

  /// \brief Checks cmd timeout for each thruster against ros time
  void checkProcessStatus(const ros::TimerEvent& event);
};

namespace ThrusterStateVariables {
    const int8_t OffPwmValue = 0x00;
    const int8_t db2 = 0x02;
    const int8_t db3 = 0x03;
    const int8_t db4 = 0x04;
    const int8_t DLE = 0x10;
    const int8_t SOH = 0x01;
    const int8_t STX = 0x02;
    const int8_t ETX = 0x03;
    const int8_t Status = 0x37;
    const int8_t WriteCmd = 0x41;
    const int8_t disable = 0x01;
    const int8_t OpenLoopPwm = 0x20;
    const int8_t ClosedLoop = 0x21;
    const int8_t BroadcastAddr = 0x6F;
    /*
    const uint8_t disable_chksum_all = 0xDE;
    const uint8_t disable_chksum_port_aft = 0xCF; //0x60
    const uint8_t      disable_chksum_stbd_aft = 0xD2; //0x63
    const uint8_t      disable_chksum_trans = 0xD0; //0x61
    const uint8_t      disable_chksum_vert = 0xD1; //0x62
    const uint8_t      disable_chksum_test = 0xD3; //0x64
    const uint8_t      off_chksum = 0x61;
     */
}; //namespace ThrusterStateVariables
} //namespace ds_thrusters

#endif  // PROJECT_THRUSTERS_H
